name := "vendingMachineMindsOnPBT"

version := "0.1"

scalacOptions ++= Seq( "-source:future", "-indent", "-rewrite")

scalaVersion := "3.0.0-RC3"

libraryDependencies += "org.scalactic" %% "scalactic" % "3.2.8"
libraryDependencies += "org.scalatest" %% "scalatest" % "3.2.8" % "test"
libraryDependencies += "org.scalacheck" %% "scalacheck" % "1.15.3" % "test"
