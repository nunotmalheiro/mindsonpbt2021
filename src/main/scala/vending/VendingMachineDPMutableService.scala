package vending

import vending.SimpleTypes.{Denomination, Money, Quantity}

import scala.annotation.tailrec


object VendingMachineDPMutableService extends VendingMachine:
  
  def arrayToMap(ad: Array[Int], aq: Array[Int]): Option[Map[Denomination,Quantity]] =
    val arr: Array[(Denomination,Quantity)] = new Array(ad.length)
    var i = 0
    while (i < ad.length)
      arr(i) = (Denomination.values.find(_.value.to ==ad(i)).get, Quantity.from(aq(i)).get)
      i += 1
    Some(arr.toMap.filter( (_, q) => q.to > 0 ) )
    

  /*** Fill the map, return the map value at the needed change
   * 
   * @param change  The Money that is to be given as change
   * @param me      The Map which represents all the Denomination inside the Venving MAchine
   * @return        The Map of the change to be given, if possible
   */
  def calculateChange(change: Money, me: Map[Denomination,Quantity]): Option[Map[Denomination,Quantity]] =
    
    val c: Int = change.to
    val ad = me.map( (d, _) => d.value.to).toArray
    val aeq = me.map( (_,q) => q.to).toArray
    
    val aai: Array[Array[Int]] = new Array(c + 1)
    aai(0) = new Array(ad.length)
    val aqd: Array[Int] = new Array(c + 1)
    aqd(0) = 0
    val aae: Array[Array[Int]] = new Array(c + 1)
    aae(0) = aeq
    
    var m = 1
    while (m <= c)
      var minQDen = Int.MaxValue
      var iden = Int.MaxValue
      var i = 0
      while (i < ad.length)
        val d = ad(i)
        if (m >= d)
          val pd = m - d
          if ((aai(pd) != null) && (aae(pd)(i) > 0))
            val qd = aqd(pd)
            if (qd < minQDen)
              minQDen = qd
              iden = i
        i += 1
      if (minQDen < Int.MaxValue)
        val pd = m - ad(iden)
        aai(m) = aai(pd).clone
        aai(m)(iden) += 1
        aqd(m) = aqd(pd) + 1
        aae(m) = aae(pd).clone
        aae(m)(iden) -= 1
      m += 1
    
    val aqr = aai(c)
    if (aqr==null) None else arrayToMap(ad, aqr)
  
