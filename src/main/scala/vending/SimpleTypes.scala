package vending

import scala.annotation.targetName

object SimpleTypes:
  
  opaque type Money = Int
  object Money:
    def unit: Money = 1
    def zero: Money = 0
    def from(i: Int): Option[Money] = if (i<0) None else Some(i)
  extension (m: Money)
    @targetName("toMoney")
    def to: Int = m
    def <(m2: Money): Boolean = m < m2
    def <=(m2: Money): Boolean = m <= m2
    def >(m2: Money): Boolean = m > m2
    def >=(m2: Money): Boolean = m >= m2
    def +(m2: Money): Money = m + m2
    def -(m2: Money): Money = m - m2
    def *(q: Quantity): Money = m * q
    def /(d: Denomination): Quantity = m / d.value
    @targetName("incMoney")
    def inc : Money = m + 1
    @targetName("decMoney")
    def dec : Option[Money] = if (m>0) Some(m - 1) else None
    @targetName("isZeroMoney")
    def isZero: Boolean = m == 0.0
  
  opaque type Quantity = Int
  object Quantity:
    def unit: Quantity = 1
    def zero: Quantity = 0
    def from(i: Int): Option[Quantity] = if (i<0) None else Some(i)
  extension (q: Quantity)
    @targetName("plusQuantity")
    def +(q2: Quantity): Quantity = q + q2
    @targetName("minusQuantity")
    def -(q2: Quantity): Option[Quantity] = 
      val qr = q - q2
      if (qr<0) None else Some(qr)

    @targetName("toQuantity")
    def to: Int = q
    @targetName("isZeroQuantity")
    def isZero: Boolean = q == 0
    infix def min(q2: Quantity): Quantity = math.min(q, q2)
    @targetName("incQuantity")
    def inc: Quantity = q + 1
    @targetName("decQuantity")
    def dec: Option[Quantity] = if (q>0) Some(q - 1) else None 
  
  enum Denomination(val value: Money):
    case oneCent extends Denomination(1)
    case twoCent extends Denomination(2)
    case fiveCent extends Denomination(5)
    case tenCent extends Denomination(10)
    case twentyCent extends Denomination(20)
    case fiftyCent extends Denomination(50)
    case oneEur extends Denomination(100)
    case twoEur extends Denomination(200)

