package vending

import SimpleTypes.{Denomination, Money, Quantity}

object VendingMachineHybridService extends VendingMachine:

  def calculateChange(change: Money, me: Map[Denomination,Quantity]): Option[Map[Denomination,Quantity]] = 
    val o = VendingMachineGreedyService.calculateChange(change,me)
    if (o.isDefined) o else VendingMachineDPService.calculateChange(change,me)
  
