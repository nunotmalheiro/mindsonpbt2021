package vending

import scala.language.adhocExtensions

import org.scalacheck.*
import org.scalacheck.Test.Parameters
import org.scalacheck.Prop.forAll
import vending.SimpleTypes.*

object VendingMachineProperties extends Properties("VendingMachineServiceProperties"):
  // maximum quantity of any Denomination
  val MAX_QUANTITY = 50
  val MAX_DENOMINATIONS = 50
  // vending machine service to test
  val VendingMachineService = VendingMachineDPService
  
  override def overrideParameters(prms: Test.Parameters): Test.Parameters =
    prms.withMinSuccessfulTests(200)

  def genQuantity: Gen[Quantity] =
    for
      iq  <-  Gen.choose(1, MAX_QUANTITY)
      q   <-  Quantity.from(iq).fold(Gen.fail)(q => Gen.const(q))
    yield q

  def genNonEmptyDenominationsList: Gen[List[Denomination]] =
    for
      n    <-    Gen.chooseNum(1, Denomination.values.size)
      ld   <-    Gen.pick(n, Denomination.values)
    yield ld.toList

  def genNonEmptyChangeMap: Gen[Map[Denomination, Quantity]] =
    for
      ld   <-    genNonEmptyDenominationsList
      lq   <-    Gen.listOfN(ld.size, genQuantity)
    yield ld.zip(lq).toMap
    
  def genMapAndDrop: Gen[(Map[Denomination, Quantity],Int)] =
    for
      m   <-  genNonEmptyChangeMap  
      d   <-  Gen.choose(1, m.size)
    yield (m,d)
    
  def mapToMoney(m: Map[Denomination, Quantity]): Money =
    m.foldLeft(Money.zero){ case (m, (d,q)) => m + d.value * q}

  def merge(m1: Map[Denomination, Quantity], m2: Map[Denomination, Quantity]): Map[Denomination, Quantity] =
    m1 ++ m2.map( (d,q) => d -> (q + m1.getOrElse(d, Quantity.zero)))  

  property("Exact Change") = forAll(genNonEmptyChangeMap) ( m =>
    val money = mapToMoney(m)
    val opm = VendingMachineService.calculateChange(money, m)
    opm.fold(false)(rm => rm == m)
  )

  property("More than Change") = forAll(genNonEmptyChangeMap,genNonEmptyChangeMap) ( (m1,m2) =>
    val c = mapToMoney(m1)
    val opm = VendingMachineService.calculateChange( c, merge(m1,m2) )
    opm.fold(false)(rm => mapToMoney(rm) == c )
  )  

  property("Less than Change") = forAll(genMapAndDrop) (  (m,d) =>
    val money = mapToMoney(m)
    val opm = VendingMachineService.calculateChange(money, m.drop(d))
    opm.fold(true)(_ => false)
  )
